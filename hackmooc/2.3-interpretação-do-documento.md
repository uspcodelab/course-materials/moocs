# Interpretação do documento

## Intro

* [x] Identação
* [x] Tag **head** e **title**
* [x] Interpretação
* [x] Árvore genealógica
* [x] Código exemplo
* [x] Desenho do DOM

---

## Identação
Indentação são os espaços que colocamos entre a borda esquerda da tela e o
início da linha de código. Fazemos isso para deixar o código mais legível, e
pra ele ter uma estrutura mais clara de onde estão as tags.

```html
<html>
<head>
<title> USPCodeLab! </title>
</head>
<body>
<h1> Hello </h1>
<p> World! </p>
</body>
</html>
```

```html
<html>
  <head>
    <title> USPCodeLab! </title>
  </head>
  <body>
    <h1> Hello </h1>
    <p> World! </p>
  </body>
</html>
```
Você pode ver que o segundo exemplo é muito mais fácil de entender o que está
acontencendo no nosso código. Podemos ver claramente que as tags `<h1>` e `<p>`
estão dentro da tag `<body>`, assim como `<title>` está dentro da `<head>`.

## Tag `<head>`
No exemplo anterior, falamos de uma tag nova, a `<head>`, essa tag contém
informações sobre a configuração da nossa página, como: o título que aparece
na aba do navegador, qual o conjunto de caractéres que vamos usar na página,
o conjunto de caractéres chineses, latinos ou russos, incluir novas bibliotecas
de javascript.

No exemplo acima usamos a tag `<title>` que defina o texto da aba do navegador.
No caso do código, estaria escrito *USPCodeLab!* na aba.

## Interpretação
Agora já vimos um pouco de código, você deve estar se perguntando: **"Como o
browser sabe o que tem que ser mostrado na tela?"**. Vocês puderam ver que o
código em HTML tem uma estrutura bem definida.

Podemos que ver que existe uma hierarquia entre as tags através da identação do
documento. Por exemplo, podemos falar que a tag `<html>` é ancestral de todas as
outras tags, ou a tag `<body>` é pai das tags `<h1>` e `<p>`. Assim podemos começar
a estruturar uma árvore genealógica das tags.

```
          html
           |
      -----+-----.
      |          |
      v          v
     body       head
      |          |
   ---+---       v
   |     |     title
   v     v
  h1     p
```

Essa estrutura de árvore é montada pelo browser conforme ele vai passando pelo
documento, e depois disso ele percorre essa árvore e montando o código em
elementos visuais na página.
