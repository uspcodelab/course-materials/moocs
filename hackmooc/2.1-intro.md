# Intro

* [x] história
* [x] significado
* [x] contraste

## História

No CERN, pelo fim da década de 1980 e começo dos anos 90, um pesquisador chamado
Tim Berners Lee estava incomodado por não conseguir facilmente compartilhar seus
trabalhos com os demais pesquisadores, porque até então as máquinas não
compartilhavam o mesmo sistema operacional, cada formato era diferente e a 
comunicação entre duas máquinas era precária -- não bastava salvar num pendrive 
ou enviar pelo email.

Com isso, Lee resolveu desenvolver um protocolo e um formato que
padronizassem a comunicação pela internet, rede que já existia. Eis que surgiu
o formato HTML e o protocolo HTTP para a troca de HTML na rede.

Desde seu surgimento, o HTML já evoluiu um bocado e atualmente o HTML já está na
versão 5, que conta com diversos recursos modernos e
interessantes, condizente com a riquesa do acesso de hoje em dia, que vai desde
os tradicionais desktops até celulares com necessidades de
responsividade e leitores de tela para pessoas com deficiência visual.


## Definição

```plain
HTML

H yper
T ext
M arkup
L anguage
```

HTML é uma linguagem de marcação utilizada para definir a estrutura de um
documento para a web. A título de curiosidade, o protocolo HTTP define a
transferência deste tipo de arquivo: HyperText Transfer Protocol.
Quando você escreve um documento HTML, você faz marcações num texto puro para
transformá-lo em um **hipertexto**. Veja a comparação abaixo:


## Texto puro vs. Hipertexto

```html
<pre>
minha terra tem palmeiras
onde canta o sabiá
as aves que aqui gorjeiam
não gorjeiam como lá
</pre>

<p>
  minha terra tem <em>palmeiras</em>
  onde canta o <em>sabiá</em>
  as aves que aqui <em>gorjeiam</em>
  não gorjeiam como <em>lá</em>
</p>
```
