# Box-Model (modelo de caixas)

Quando estamos pensando no design de páginas Web, é importante lembrar que 
cada elemento que constitui a página deve ser pensado como sendo uma "caixa". 
Essa percepção torna o design baseado em CSS muito mais intuitivo.

Quando tratamos dessas "caixas", é importante também entendermos como elas são 
desenhadas e como sua _altura_ e _largura_ é calculada. A imagem abaixo 
ilustra bem as diferentes "camadas" que compõem essas caixas:

[https://css-tricks.com/wp-content/csstricks-uploads/thebox.png]

Como podemos ver, as caixas possuem propriedades de _alture_ e _largura_ 
internas, além de uma camada de _padding_, que podem ser vistas também como "
margens internas". A caixa tem também uma _borda_, e finalmente uma _margem_. 
A margem não tem efeito algum na parte interna da caixa, mas não pode ser 
ignorada porque afeta os elementos à sua volta.

Para trabalharmos com essas diferentes "camadas", basta utilizar atributos no 
CSS:

```css
.class1 {
  padding: 1em;
  margin: 3rem;
  height: 40vh;
  width: 70vw;
}
```

Vale notar que quando atribuímos valores para _padding_ e _margin_, estamos 
definindo um único valor para todas as direções (_top_, _right_, _bottom_, 
_left_). Essas propriedades podem ser setados individualmente para cada direção
, com a mesma facilidade:

```css
.class1 {
  padding-top: 1em;
  padding-bottom: 2em;
  padding-right: 3em;
  padding-left: 2em;
  margin-top: 1rem;
  margin-bottom: 2rem;
  margin-left: 3rem;
  margin-right: 4rem;
  height: 40vh;
  width: 70vw;
}
```

O CSS também nos permite ecomizar linhas com um jeito mais curto de setar 
todos os atributos em uma única linha da seguinte maneira: `padding: 1em 3em 
2em 2em`, onde os valores são interpretados na seguinte ordem: `top, right, 
bottom, left`. O mesmo padrão funciona para setar as margens. Além disso, não 
precisamos repetir valores, escrevendo menos ainda quando vamos utilizar 
valores repetidos em um mesmo eixo: `padding: 1em 2em 1em 2em` é a mesma coisa 
que escrever `padding: 1em 2em`. Novamente, essa maneira de escrever é válida 
para `margin` também.
