# HackMOOC

Curso de introdução ao desenvolvimento web


## Roadmap

0. Intro ao curso
    1. história
    2. arquitetura
1. Setup
    1. Linux
    2. Windows
    3. mac 
2. HTML
    1. Intro
        - história
        - significado
        - motivação/contraste
    2. fazendo marcações
        - parágrafos
        - títulos
        - links
        - imagens
        - listas
    3. Interpretação do documento
        - documento vs. estrutura interpretada
    4. Agrupamento e Semântica
        - tags semânticas
        - divs (fallback)
3. CSS básico
    1. Intro
        - contraste
        - posicionamento
        - alguns atributos
    2. Cascateamento
        - hierarquia
        - relação com DOM
    3. Box Model
        -  primeiras medidas
    4. classes e B.E.M.
4. CSS com Flexbox (??)
5. CSS com Grid (??)
6. Responsividade (?)
    1. Introdução (mobile-first)
    2. `@media`
        - medidas responsivas
        - flex+grid
7. Versionamento e Entrega
    1. git
    2. GitLab / GitHub
    3. Deploy
8. JavaScript pt1
    1. Intro + contexto histórico
    2. Variáveis e Data Types
    3. Controle de fluxo
    4. Loops
    5. Funções
    6. JSON
    7. Debugging no Chrome + Comentários
9. JavaScript pt2
    1. DOM
    2. Nodes no DOM
    3. Percorrer e buscar no DOM
    4. Modificar e interagir com DOM
    5. Coords+Anim (cool scroll things)
10. JavaScript pt3
    1. introdução aos eventos do browser
    2. bubbling and capturing
    3. delegação
    4. comportamento padrão
    5. eventos personalizados

** projeto precisa ser pensado para ver como ele se encaixa no curso **

## Autores

- João Daniel
- Leonardo Lana
- Rafael Rahal
- Victor Hugo
